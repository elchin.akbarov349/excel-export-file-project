package com.example.demoexportproject.converter;

import com.example.demoexportproject.entity.CashierReportDto;
import com.example.demoexportproject.entity.CashierTicketReportDto;
import com.example.demoexportproject.model.TableMetadata;
import com.example.demoexportproject.model.TableMultiple;
import com.example.demoexportproject.model.TableResult;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class ExportConverter {

    public TableMultiple convert(List<CashierReportDto> cashierReportDtos) {
        TableMultiple tableMultiple = new TableMultiple();
        tableMultiple.setTableName("Financial Performance");
        List<TableResult> tableResultList = new ArrayList<>();
        tableResultList.add(convertAgent(cashierReportDtos));
        tableResultList.add(convertCashiers(getAllCashiers(cashierReportDtos)));
        tableMultiple.setTableResultList(tableResultList);
        return tableMultiple;
    }

    private TableResult convertAgent(List<CashierReportDto> cashierReportDtos) {
        List<String> columns = Arrays.asList(
                "AGENT ID", "AGENT CODE", "TURNOVER", "PAYOUT"
        );
        TableResult tableResult = new TableResult();
        TableMetadata metadata = new TableMetadata();
        metadata.setColumns(columns);
        tableResult.setMetadata(metadata);
        tableResult.setSheet("Agent Financial");

        Object[][] rows = new Object[cashierReportDtos.size()][columns.size()];
        IntStream.range(0, cashierReportDtos.size()).forEach(
                i -> {
                    CashierReportDto dto = cashierReportDtos.get(i);
                    rows[i][0] = dto.getAgentId();
                    rows[i][1] = dto.getAgentCode();
                    rows[i][2] = dto.getTurnOver();
                    rows[i][3] = dto.getPayout();
                }
        );
        tableResult.setRows(rows);
        return tableResult;
    }

    private TableResult convertCashiers(List<CashierTicketReportDto> cashierTicketReportDtos) {
        List<String> columns = Arrays.asList(
                "CASHIER ID", "CASHIER CODE", "AGENT ID", "TURNOVER", "PAYOUT"
        );
        TableResult tableResult = new TableResult();
        TableMetadata metadata = new TableMetadata();
        metadata.setColumns(columns);
        tableResult.setMetadata(metadata);
        tableResult.setSheet("Cashier Financial");

        Object[][] rows = new Object[cashierTicketReportDtos.size()][columns.size()];
        IntStream.range(0, cashierTicketReportDtos.size()).forEach(
                i -> {
                    CashierTicketReportDto dto = cashierTicketReportDtos.get(i);
                    rows[i][0] = dto.getCashierId();
                    rows[i][1] = dto.getCashierCode();
                    rows[i][2] = dto.getAgentId();
                    rows[i][3] = dto.getTurnOver();
                    rows[i][4] = dto.getPayout();
                }
        );
        tableResult.setRows(rows);
        return tableResult;
    }

    private List<CashierTicketReportDto> getAllCashiers(List<CashierReportDto> cashierReportDtoList) {
        List<CashierTicketReportDto> cashierList = new ArrayList<>();
        cashierReportDtoList.forEach(m -> {
            cashierList.addAll(m.getCashiers());
        });
        return cashierList;
    }
}
