package com.example.demoexportproject.entity;

import lombok.Data;

@Data
public class CashierTicketReportDto {
    private Long cashierId;
    private String cashierCode;
    private Long agentId;
    private Double turnOver=0.00;
    private Double payout=0.00;
}
