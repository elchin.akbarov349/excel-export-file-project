package com.example.demoexportproject.entity;

import lombok.Data;

import java.util.List;

@Data
public class CashierReportDto {
    private Long agentId;
    private String agentCode;
    private Double turnOver=0.00;
    private Double payout=0.00;
    private List<CashierTicketReportDto> cashiers;
}
