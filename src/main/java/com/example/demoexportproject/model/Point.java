package com.example.demoexportproject.model;

import lombok.Data;

@Data
public class Point {
    private static final long serialVersionUID = 8127354804927478508L;
    private Integer x;
    private Integer y;

}
