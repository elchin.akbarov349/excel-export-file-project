package com.example.demoexportproject.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TableMetadata implements Serializable {
    private static final long serialVersionUID = -8097289130330605352L;
    private List<String> columns;
    private List<Integer> highlightedColumns;
    private List<Point> highlightedCells;
}
