package com.example.demoexportproject.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TableMultiple implements Serializable {
    private static final long serialVersionUID = -3354586044862051347L;
    private String tableName;
    private List<TableResult> tableResultList;
}
