package com.example.demoexportproject.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class TableResult implements Serializable {
    private static final long serialVersionUID = 2913253710694144872L;
    private TableMetadata metadata;
    private String sheet;
    private Object[][] rows;
    private Map<String, Object[]> details;
}
