package com.example.demoexportproject.controller;

import com.example.demoexportproject.converter.ExportConverter;
import com.example.demoexportproject.entity.CashierReportDto;
import com.example.demoexportproject.export.ExcelExportFile;
import com.example.demoexportproject.model.TableMultiple;
import com.example.demoexportproject.staticdata.GetStaticData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class ExportController {

    private final ExcelExportFile excelExportFileV2;
    private final ExportConverter exportConverter;

    public ExportController(ExcelExportFile excelExportFileV2, ExportConverter exportConverter) {
        this.excelExportFileV2 = excelExportFileV2;
        this.exportConverter = exportConverter;
    }

    @GetMapping
    public void download(HttpServletResponse httpServletResponse) throws IOException {
        List<CashierReportDto> cashierReportDtoList = GetStaticData.getCashierReportDtoList();
        TableMultiple tableMultiple = exportConverter.convert(cashierReportDtoList);
        excelExportFileV2.getHttpHeaders(tableMultiple).forEach(
                httpServletResponse::setHeader
        );
        excelExportFileV2.export(tableMultiple, httpServletResponse.getOutputStream());
        httpServletResponse.getOutputStream().flush();
    }
}
