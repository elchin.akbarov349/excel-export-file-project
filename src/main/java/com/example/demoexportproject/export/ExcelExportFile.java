package com.example.demoexportproject.export;

import com.example.demoexportproject.model.Point;
import com.example.demoexportproject.model.TableMetadata;
import com.example.demoexportproject.model.TableMultiple;
import com.example.demoexportproject.model.TableResult;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

@Component
public class ExcelExportFile {

    public Map<String, String> getHttpHeaders(TableMultiple tableMultiple) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/octet-stream");
        String contentDisposition = String.format("attachment; filename=\"%s-%s.xlsx\"",
                tableMultiple.getTableName(),
                LocalDateTime.now(Clock.systemUTC()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm"))
        );
        headers.put("Content-Disposition", contentDisposition);
        return headers;
    }
    public void export(TableMultiple tableMultiple, OutputStream outputStream) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        tableMultiple.getTableResultList().forEach(m -> {
            try {
                execute(workbook, m);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        workbook.write(outputStream);
        workbook.close();
        System.out.println("Successfully created");
    }

    private void execute(XSSFWorkbook workbook, TableResult tableResult) throws IOException {
        TableMetadata metadata = tableResult.getMetadata();
        XSSFSheet sheet = workbook.createSheet(tableResult.getSheet());
        XSSFFont headerFont = getHeaderFont(workbook);
        XSSFCellStyle headerStyle = getHeaderStyle(workbook, headerFont);
        XSSFCellStyle highlightedHeaderStyle = getHighlightedHeaderStyle(workbook, headerFont);
        XSSFFont cellFont = workbook.createFont();
        XSSFCellStyle cellStyle = getCellStyle(workbook, cellFont);
        XSSFCellStyle highlightedCellStyle = getHighlightedCellStyle(workbook, cellFont);
        XSSFRow header = sheet.createRow(0);
        executeColumns(metadata, header, highlightedHeaderStyle, headerStyle);
        executeRows(tableResult, sheet, metadata, highlightedCellStyle, cellStyle);
        autoSize(metadata, sheet);
    }

    private XSSFCellStyle getHeaderStyle(XSSFWorkbook workbook, XSSFFont headerFont) {
        XSSFCellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFont(headerFont);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.index);
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        headerStyle.setBorderLeft(BorderStyle.THIN);
        headerStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return headerStyle;
    }

    private XSSFCellStyle getHighlightedHeaderStyle(XSSFWorkbook workbook, XSSFFont headerFont) {
        XSSFCellStyle highlightedHeaderStyle = workbook.createCellStyle();
        highlightedHeaderStyle.setFont(headerFont);
        highlightedHeaderStyle.setAlignment(HorizontalAlignment.CENTER);
        highlightedHeaderStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.index);
        highlightedHeaderStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        highlightedHeaderStyle.setBorderTop(BorderStyle.DOTTED);
        highlightedHeaderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        highlightedHeaderStyle.setBorderBottom(BorderStyle.DOTTED);
        highlightedHeaderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        highlightedHeaderStyle.setBorderRight(BorderStyle.DOTTED);
        highlightedHeaderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        highlightedHeaderStyle.setBorderLeft(BorderStyle.DOTTED);
        highlightedHeaderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return highlightedHeaderStyle;
    }

    private XSSFCellStyle getCellStyle(XSSFWorkbook workbook, XSSFFont cellFont) {
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(cellFont);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        cellStyle.setBorderLeft(BorderStyle.THIN);
        cellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return cellStyle;
    }

    private XSSFCellStyle getHighlightedCellStyle(XSSFWorkbook workbook, XSSFFont cellFont) {
        XSSFCellStyle highlightedCellStyle = workbook.createCellStyle();
        highlightedCellStyle.setFont(cellFont);
        highlightedCellStyle.setAlignment(HorizontalAlignment.CENTER);
        highlightedCellStyle.setBorderTop(BorderStyle.DOTTED);
        highlightedCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
        highlightedCellStyle.setBorderBottom(BorderStyle.DOTTED);
        highlightedCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        highlightedCellStyle.setBorderRight(BorderStyle.DOTTED);
        highlightedCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
        highlightedCellStyle.setBorderLeft(BorderStyle.DOTTED);
        highlightedCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        return highlightedCellStyle;
    }

    private XSSFFont getHeaderFont(XSSFWorkbook workbook) {
        XSSFFont headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.WHITE.index);
        return headerFont;
    }

    private void executeColumns(TableMetadata metadata, XSSFRow header,
                                XSSFCellStyle highlightedHeaderStyle, XSSFCellStyle headerStyle) {
        for (int i = 0; i < metadata.getColumns().size(); i++) {
            XSSFCell cell = header.createCell(i);
            if (metadata.getHighlightedColumns() != null
                    && metadata.getHighlightedColumns().contains(i)) {
                cell.setCellStyle(highlightedHeaderStyle);
            } else {
                cell.setCellStyle(headerStyle);
            }
            cell.setCellValue(metadata.getColumns().get(i));
        }
    }

    private void executeRows(TableResult tableResult, XSSFSheet sheet, TableMetadata metadata,
                             XSSFCellStyle highlightedCellStyle, XSSFCellStyle cellStyle) {
        for (int i = 0; i < tableResult.getRows().length; i++) {
            XSSFRow row = sheet.createRow(i + 1);
            for (int j = 0; j < tableResult.getRows()[i].length; j++) {
                XSSFCell cell = row.createCell(j);
                Object obj = tableResult.getRows()[i][j];
                if (obj == null) {

                } else if (obj instanceof Number) {
                    cell.setCellValue(((Number) obj).doubleValue());
                } else {
                    cell.setCellValue(obj.toString());
                }

                if (metadata.getHighlightedCells() != null) {
                    Point currentPoint = new Point();
                    currentPoint.setX(j);
                    currentPoint.setY(i);
                    if (metadata.getHighlightedCells().contains(currentPoint)) {
                        cell.setCellStyle(highlightedCellStyle);
                        continue;
                    }
                }
                cell.setCellStyle(cellStyle);
            }
        }
    }

    private void autoSize(TableMetadata metadata, XSSFSheet sheet) throws IOException {
        IntStream.range(0, metadata.getColumns().size())
                .forEach(sheet::autoSizeColumn);
    }
}
