package com.example.demoexportproject.staticdata;

import com.example.demoexportproject.entity.CashierReportDto;
import com.example.demoexportproject.entity.CashierTicketReportDto;

import java.util.ArrayList;
import java.util.List;

public class GetStaticData {

    public static List<CashierReportDto> getCashierReportDtoList() {
        CashierTicketReportDto ticketReportDto = new CashierTicketReportDto();
        ticketReportDto.setAgentId(1L);
        ticketReportDto.setCashierId(56L);
        ticketReportDto.setCashierCode("543112");
        ticketReportDto.setTurnOver(9912.1);
        ticketReportDto.setPayout(32.2);

        CashierTicketReportDto ticketReportDto2 = new CashierTicketReportDto();
        ticketReportDto2.setAgentId(1L);
        ticketReportDto2.setCashierId(73L);
        ticketReportDto2.setCashierCode("340201");
        ticketReportDto2.setTurnOver(9122.13);
        ticketReportDto2.setPayout(514.1);

        CashierTicketReportDto ticketReportDto3 = new CashierTicketReportDto();
        ticketReportDto3.setAgentId(1L);
        ticketReportDto3.setCashierId(43L);
        ticketReportDto3.setCashierCode("130201");
        ticketReportDto3.setTurnOver(3542.13);
        ticketReportDto3.setPayout(491.1);

        List<CashierTicketReportDto> cashierTicketReportDtoList = new ArrayList<>();
        cashierTicketReportDtoList.add(ticketReportDto);
        cashierTicketReportDtoList.add(ticketReportDto2);
        cashierTicketReportDtoList.add(ticketReportDto3);

        CashierTicketReportDto ticketReportDto4 = new CashierTicketReportDto();
        ticketReportDto.setAgentId(2L);
        ticketReportDto.setCashierId(126L);
        ticketReportDto.setCashierCode("105201");
        ticketReportDto.setTurnOver(132.13);
        ticketReportDto.setPayout(564.1);

        CashierTicketReportDto ticketReportDto5 = new CashierTicketReportDto();
        ticketReportDto2.setAgentId(2L);
        ticketReportDto2.setCashierId(112L);
        ticketReportDto2.setCashierCode("100101");
        ticketReportDto2.setTurnOver(12332.13);
        ticketReportDto2.setPayout(4123.1);

        List<CashierTicketReportDto> cashierTicketReportDtoList2 = new ArrayList<>();
        cashierTicketReportDtoList2.add(ticketReportDto4);
        cashierTicketReportDtoList2.add(ticketReportDto5);

        CashierReportDto reportDto = new CashierReportDto();
        reportDto.setCashiers(cashierTicketReportDtoList);
        reportDto.setAgentId(1L);
        reportDto.setAgentCode("3133");
        reportDto.setPayout(42313.12);
        reportDto.setTurnOver(3113.1);

        CashierReportDto reportDto2 = new CashierReportDto();
        reportDto2.setCashiers(cashierTicketReportDtoList2);
        reportDto2.setAgentId(2L);
        reportDto2.setAgentCode("1231");
        reportDto2.setPayout(63542.3);
        reportDto2.setTurnOver(6421.5);

        List<CashierReportDto> list = new ArrayList<>();
        list.add(reportDto);
        list.add(reportDto2);
        return list;
    }
}
